## Overview
NoMobLag **dynamically reduces** lag from **entity tick** based on the current **server TPS**. It serves as an all-in-one replacement for many mob-management plugins, making it extremely useful for large servers with multiple player-built mob farms that lag the server.

## Wiki
[Features](https://gitlab.com/Zenya4/no-mob-lag/-/wikis/Features)<br>
[Commands](https://gitlab.com/Zenya4/no-mob-lag/-/wikis/Commands)<br>
[Permissions](https://gitlab.com/Zenya4/no-mob-lag/-/wikis/Permissions)

## Files
[config.yml](https://gitlab.com/Zenya4/no-mob-lag/-/tree/master/core/src/main/resources/config.yml)<br>
[messages.yml](https://gitlab.com/Zenya4/no-mob-lag/-/tree/master/core/src/main/resources/messages.yml)

## More info
[Download](https://www.spigotmc.org/resources/%E2%9D%8C%E2%98%A2%EF%B8%8F-nomoblag-%E2%98%A2%EF%B8%8F%E2%9D%8C-entity-farm-limiter-spawner-nerf-mob-freezing-more.72878/) (SpigotMC)<br>
[Support](https://discord.gg/KGuaxpM) (Discord)

## Credits
This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.<br><br>
Copyright © Zenya4 ([Zenya#0093](https://discord.gg/KGuaxpM) on Discord)